#KeyWords
CREATE TABLE KeyWord (
  keyWordId  INT PRIMARY KEY,
  keWordText VARCHAR(15)
);
CREATE TABLE KeyWordRefernce (
  KeyWordReferenceID INT PRIMARY KEY,
  KeyWordId          INT,
  FOREIGN KEY (KeyWordId) REFERENCES KeyWord (keyWordId)
);
#einde keywords
CREATE TABLE City (
  cityID     INT PRIMARY KEY,
  city       VARCHAR(30) NOT NULL,
  postalCode INT(4)      NOT NULL
);
CREATE TABLE Personage (
  personageID INT PRIMARY KEY,
  cityID      INT,
  FOREIGN KEY (cityID) REFERENCES City (cityID)
);
# personaVragen
CREATE TABLE PersonaAnswer (
  personaAnswerID INT PRIMARY KEY,
  answerKeyWord   INT,
  answertext      VARCHAR(100),
  FOREIGN KEY (answerKeyWord) REFERENCES KeyWordRefernce (KeyWordReferenceID)
);
CREATE TABLE PersonaQuestion (
  questionID     INT PRIMARY KEY,
  personaAnswer1 INT,
  personaAnswer2 INT,
  questionText   VARCHAR(100) NOT NULL,
  FOREIGN KEY (personaAnswer1) REFERENCES PersonaAnswer (personaAnswerID),
  FOREIGN KEY (personaAnswer2) REFERENCES PersonaAnswer (personaAnswerID)
);
CREATE TABLE personaLineItem (
  personaLineItemID INT PRIMARY KEY,
  personaQuestionID INT,
  personaAnswerID   INT,
  personageID       INT REFERENCES Personage (personageID),
  FOREIGN KEY (personaQuestionID) REFERENCES PersonaQuestion (questionID),
  FOREIGN KEY (personaAnswerID) REFERENCES PersonaAnswer (personaAnswerID)
);
#einde personavragen
CREATE TABLE Status (
  stuatusID   INT PRIMARY KEY,
  personageID INT,
  FOREIGN KEY (personageID) REFERENCES Personage (personageID)
);
CREATE TABLE SituationOption (
  optionID      INT PRIMARY KEY,
  optionText    VARCHAR(100),
  failChance    DOUBLE(4, 3),
  optionKeyWord INT,
  FOREIGN KEY (optionKeyWord) REFERENCES KeyWordRefernce (KeyWordReferenceID)
);
CREATE TABLE Situation (
  situationID   INT PRIMARY KEY,
  situationText VARCHAR(100),
  option1       INT,
  option2       INT,
  FOREIGN KEY (option1) REFERENCES SituationOption (optionID),
  FOREIGN KEY (option2) REFERENCES SituationOption (optionID)
);
CREATE TABLE SituationLineItem (
  situatieLineItemID INT PRIMARY KEY,
  situationID        INT,
  chosenOptionId     INT,
  statusID           INT,
  FOREIGN KEY (situationID) REFERENCES Situation (situationID),
  FOREIGN KEY (chosenOptionId) REFERENCES SituationOption (optionID),
  FOREIGN KEY (statusID) REFERENCES Status (stuatusID)
);
CREATE TABLE Verdict (
  verdictID     INT PRIMARY KEY,
  verdictText   VARCHAR(100) NOT NULL,
  statusID INT,
  lastSituationLineID INT,
  FOREIGN KEY (lastSituationLineID) REFERENCES SituationLineItem(situatieLineItemID),
  FOREIGN KEY (statusID) REFERENCES Status (stuatusID)
);
#politic opinions
CREATE TABLE Admin (
  adminID      INT PRIMARY KEY,
  fname        VARCHAR(20) NOT NULL,
  lname        VARCHAR(20) NOT NULL,
  emailAddress VARCHAR(20) NOT NULL,
  cityID       INT,
  FOREIGN KEY (cityID) REFERENCES City (cityID)
);

CREATE TABLE Policy (
  policyID    INT PRIMARY KEY,
  situationID INT,
  policyText  VARCHAR(100),
  FOREIGN KEY (situationID) REFERENCES Situation (situationID)
);
CREATE TABLE Party (
  partyID   INT PRIMARY KEY,
  partyName VARCHAR(20) NOT NULL,
  email     VARCHAR(20) NOT NULL,
  credo     VARCHAR(100),
  location  INT,
  partOf    INT,
  createdBy INT,
  FOREIGN KEY (location) REFERENCES City (cityID),
  FOREIGN KEY (partOf) REFERENCES Party (partyID),
  FOREIGN KEY (createdBy) REFERENCES Admin (adminID)
);
CREATE TABLE PoliticalPosition (
  positionID   INT PRIMARY KEY,
  positionText VARCHAR(100) NOT NULL,
  policyID     INT,
  supportedBy  INT,
  FOREIGN KEY (policyID) REFERENCES Policy (policyID),
  FOREIGN KEY (supportedBy) REFERENCES Party (partyID)
);
